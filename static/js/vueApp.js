axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
Vue.use(BootstrapVue);

document.onload = $("#esconde").click(function () {
  $("#app").toggle();
});

var app1 = new Vue({
  el: "#app",
  delimiters: ["[[", "]]"],
  data() {
    return {
      files: [],
      contenido: null,
      loading: null,
      errored: null,
    };
  },
  methods: {
    addFiles() {
      this.$refs.files.click();
    },
    submitFiles() {
      this.loading = true;
      this.contenido = null;
      setInterval(function () {
        $("#loading").toggle();
      }, 250);
      let formData = new FormData();
      for (var i = 0; i < this.files.length; i++) {
        let file = this.files[i];
        formData.append('image' + i, file);
      }
      axios
        .post("/", formData, {})
        .then((response) => (this.contenido = response.data))
        .catch(function () {
          console.log("FAILURE!!");
          this.errored = true;
        })
        .finally(() => (this.loading = false));
    },
    handleFilesUpload() {
      let uploadedFiles = this.$refs.files.files;
      for (var i = 0; i < uploadedFiles.length; i++) {
        this.files.push(uploadedFiles[i]);
      }
    },
    removeFile(key) {
      this.files.splice(key, 1);
    },
  },
});


/**
 * Vue class to process text asyncronously
  */

 document.onload = $("#esconde2").click(function () {
  $("#app2").toggle();
});


var app2 = new Vue({
  el: "#app2",
  delimiters: ["[[", "]]"],
  data() {
    return {
      text: [],
      contenido_texto: null,
      errored:null,
      loading:false
    };
  },
  methods: {
    submitText() {
      this.loading = true;
      this.contenido_texto = null;
      setInterval(function () {
        $("#loading").toggle();
      }, 250);
      let formData = $('#sentiment').val();
      console.log(formData);
      axios
        .post("/sentiment/", formData, {})
        .then((response) => (this.contenido_texto = response.data))
        .catch(function () {
          console.log("FAILURE!!");
          this.errored = true;
        })
        .finally(() => (this.loading = false));
    },
  },
});
