from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views import View
from django.http import HttpResponse
import numpy as np
from PIL import Image
import io
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
from keras.applications.vgg16 import VGG16
from keras.models import load_model
from keras.backend import clear_session
import pandas as pd
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences


def load_image_model():
    model = VGG16()
    return model


def load_text_model():
    clear_session()
    model = load_model('./nlp_model_hard.h5') 
    return model


def load_csv():
    df= pd.read_csv('./tweets2-backup.csv', sep=',')
    df.SentimentText=df.SentimentText.astype(str)
    df.head(10)
    #select relavant columns
    tweet_df = df[['SentimentText','Sentiment']]
    tweet_df = tweet_df[tweet_df['Sentiment'] != 'neutral']
    # convert airline_seentiment to numeric
    sentiment_label = tweet_df.Sentiment.factorize()
    tweet = tweet_df.SentimentText.values
    tokenizer = Tokenizer(num_words=5000)
    tokenizer.fit_on_texts(tweet)
    vocab_size = len(tokenizer.word_index) + 1 

    return tokenizer
    
# Create your views here.
class MlVisualizer(TemplateView):
    template_name = 'index.html'
    model = load_image_model()

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})
    
    def post(self, request, *args, **kwargs):
         if request.method == 'POST':
                predictions = []
                counter = 0
                print('hello')           
                for i in request.FILES:                   
                    image = request.FILES[i].read()
                    image = Image.open(io.BytesIO(image))
                    image = image.convert('RGB')
                    image = image.resize((224,224),Image.NEAREST)
                    image = img_to_array(image)
                    image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
                    image = preprocess_input(image)
                    yhat = self.model.predict(image)
                    label = decode_predictions(yhat)
                    label = label[0][0]
                    predictions.append('%s (%.2f%%)' % (label[1], label[2]*100))
                    counter = counter + 1             
                clear_session()
                return HttpResponse(predictions)


class SentimentView(TemplateView):
    template_name = 'text.html'
    model = load_text_model()
    tokenizer = load_csv()
    
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})
    
    def post(self, request, *args, **kwargs):
         if request.method == 'POST':
            test_word = str(request.POST)         
            tw = self.tokenizer.texts_to_sequences([test_word])
            tw = pad_sequences(tw,maxlen=200)
            prediction = self.model.predict(tw)
            if prediction >= 0.55:
                prediction = "Positive"
            elif prediction >= 0.45 and prediction < 0.55:
                prediction = "Neutral sentiment"
            else:
                prediction = "Negative"
            
            return HttpResponse(prediction)