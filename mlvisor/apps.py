from django.apps import AppConfig


class MlvisorConfig(AppConfig):
    name = 'mlvisor'
